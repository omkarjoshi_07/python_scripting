# import selenium webdriver package which is going to do the job of loading websites directly
# selenium is more powerful than beautifulSoup
from selenium import webdriver
import time

# webdriver used for driving the browser
# webdriver has we need to download for specific browser.
# e.g. for chrome, we need chrome web driver similarly for forefox and so on.
from selenium.webdriver.common.keys import Keys

# to find elements by id
from selenium.webdriver.common.by import By


def function1():
    # instantiate the browser 
    # put the path of chrome driver here
    # In below code, is old vsersioned code where we don't have to specify chrome driver path
    # browser = webdriver.Chrome("C:/Users/DELL/Desktop/selenium/chromedriver.exe")
    
    # below code will take handle browser and driver itself
    browser = webdriver.Chrome()

    # open the browser to visit google.com on chrome window and automatically closes after 5 seconds
    browser.get("https://google.co.in")
    time.sleep(5)

    # search automatically on google
    # To execute below code, we need to find id element of google searchbox
    # to find id, right click on google search box and find what is the value "id" attribute
    # in this case, id="ApjFqb"
    input = browser.find_element(By.ID, "APjFqb")
    
    # search string
    input.send_keys("iPhone 13 pro")
    
    # hold for 5 seconds
    time.sleep(5)

    # make search automatically
    input.send_keys(Keys.ENTER)
    time.sleep(5)

    # Closes the browser and shuts down the ChromiumDriver executable.
    browser.quit()


# function1()


def function2():
    # instantiate the browser
    browser = webdriver.Chrome()

    # open the browser to visit google.com
    browser.get("https://www.accuweather.com/en/in/pune/204848/daily-weather-forecast/204848")
    time.sleep(2)
    
    # find element where high and low temp are. In below case element class name="temp"
    container_divs = browser.find_element(By.CLASS_NAME, "temp")

    # for div in container_divs:
    #     dow = div.find_element_by_class_name("dow")
    #     sub = div.find_element_by_class_name("sub")
    #     high = div.find_element_by_class_name("high")
    #     low = div.find_element_by_class_name("low")
    
    temps = container_divs.text.split()

    
    print(f"Low = {temps[1]}, High = {temps[0]}")

    # time.sleep(2)
    browser.close()


# function2()


def function3():
    
    # instantiate the browser
    browser = webdriver.Chrome()

    # open the browser to visit google.com
    browser.get("https://www.accuweather.com/en/in/pune/204848/daily-weather-forecast/204848")
    # time.sleep(2)
    
    # find elements where high and low temp are. In below case parent element class name="temp"
    container_divs = browser.find_elements(By.CLASS_NAME, "info")
    
    # In "info" class below "high", "low", "date" are all child classes
    # for div in container_divs:
    #     high = div.find_element(By.CLASS_NAME, "high")
    #     low = div.find_element(By.CLASS_NAME, "low")
    #     day = div.find_element(By.CLASS_NAME, "date")
    #     print(f"High temp: {high.text}, Low Temp: {low.text}, day: {day.text}\n")
        # print(day.text.replace("\n"," "))
        
    # store the details in csv file
    with open("temperature_pune.csv", "w", encoding=("utf-8")) as f:
        f.write("High Temp, Low Temp, Day\n")
        for div in container_divs:
            high = div.find_element(By.CLASS_NAME, "high")
            low = div.find_element(By.CLASS_NAME, "low")
            day = div.find_element(By.CLASS_NAME, "date").text.replace("\n", " ")
            f.write(f"{high.text}, {low.text}, {day}\n")
    

# function3()


def function4():
    # check how data is geting stored
    import pandas as pd
    df = pd.read_csv("./temperature_pune.csv")
    print(df)
    

# function4()



    
    
    