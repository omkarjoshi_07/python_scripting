# import os module
import os

def file__directory_manipulation():
    
    # to check if file or directory exists
    # providing relative path of file in below code
    # ../test.txt -> means we are checking file in previous diretory than current onlt "./" -> means current directory. These are all relative paths
    if os.path.exists("../test.txt"):
        print("File exists!")
    else:
        print("File Does not exists!")
    
    # get absolute path of current working directory
    print(os.getcwd())
        
    path = os.getcwd()
    
    # get the list of all files and directories
    directory_list = os.listdir(path)
    print(f"Directory list in current working directory: {directory_list}")
    
    # in below code, \ is escape sequence and hence either joined with one more \ or we can use single / or use of raw string
    path = r"C:\Users\DELL\Desktop\sample.csv" # Ok
    path = "C:\\Users\\DELL\\Desktop\\sample.csv" # Ok
    # or 
    path = "C:/Users/DELL/Desktop/sample.csv" # Ok
    
    # one more alternative
    path = os.path.join("C:\\", "Users", "DELL", "Desktop", "sample.csv")
    
    with open(path) as f:
        print(f.read())
    
    
    # make new directory
    # os.mkdir("./new_directory")
    
    # rename file
    # os.rename("test.txt", "test_renamed.txt")
    
    # remove file
    # os.remove("test.txt")
    
    # removing directory
    # os.rmdir("new_directory")
    

file__directory_manipulation()