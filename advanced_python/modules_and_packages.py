# import module
# import my_package

# Using packages from modules
# from my_package import account, person

# another way of importing modules account and person
from my_package.person import Person
from my_package.account import Account

# import modules with aliases
# from my_package import person as per

def function_1():
    
    # importing person module from package named my_package
    person1 = Person("Raj")
    print(person1)

    # importing account module from package named my_package
    acc1 = Account(20000.00, "Savings")
    print(acc1)

    
function_1()

def heavy_computation():
    # This can improve startup time and reduce memory usage
    import my_package
    # Use heavy_module here
    


