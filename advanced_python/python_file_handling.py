# file handling

def function_1():
    """
    write file
    """
    try:
    
        """
        This means you've opened the file for writing, which will create the 
        file if it doesn't exist or overwrite the file if it already exists.
        A file object is an object in Python that gives access to files. 
        These could be the on-disk files, meaning files in the hard disk, 
        or files present in other storage
        """
        file = open('test.txt', 'w')
        
      
        file.write("Welcome to file handling in python")
        
        """ 
        flush() -> flush used to flush internal buffer to memory.
        When you write data to file operating system buffers data into memory
        before actually writing it to the disk.
        
        """
        # file.flush()
    
    except Exception as e:
        print(f"Error occurred: {e}")
    
    finally:
        # close  resource
        file.close()    

# function_1()


def function_3():
    
    try:
        
        # read mode
        file = open('test.txt', 'r')
        # data = file.read()
    
        # print(f"file content = {data}")
    
        # file pointer to current position
        print(f"file pointer is pointing to {file.tell()}")
        print(f"all data: {file.read()}")
        
        # read next 4 bytes
        data = file.read(4)
        print(f"data = {data}")
        print(f"file pointer is pointing to {file.tell()}")
    
        data = file.read(4)
        print(f"data = {data}")
        print(f"file pointer is pointing to {file.tell()}")
    
        # set file pointer to 0
        file.seek(0)
        print(f"file pointer is pointing to {file.tell()}")
    
        data = file.read(10)
        print(f"data = {data}")
        
        # read all lines in file
        all_lines = file.readlines()
        # prints \n as it is
        print(f"all lines = {all_lines}")
        
    except Exception as e:
        print(f"Error occurred: {e}")
    
    finally:
        # close  resource
        file.close() 
    


# function_3()

def function_4():
    # using with statement, it handles closing of file automatically, unlike
    # open where we need to give close statement explicitly
    with open("test.txt", "r") as f:
        data = f.read()
        print(data)
        
# function_4()
    
    
def function_5():
    try:
        # append content to a file
        file = open('test.txt', 'a')
        file.write("\nthis is appended line")
    
    except Exception as e:
        print(f"Error occurred: {e}")
    
    finally:
        # close  resource
        
        file.close() 


# function_5()


def function_6():
    import json
    
    # using multiple "with statements"
    # In below code, we are reading two separate files one is text & other 
    # is json using single with statements. 
    with open("test.txt", "r") as f, open("config.json", "r") as f_json:
        data = f.read()
        print(f"data in test file: {data}")
        
        json_data = json.load(f_json)
        print(f"json data : {json_data}")


# function_6()        


def csv_operations():
    # import csv module
    import csv
    
    # using csv.reader() gives list of each row
    # with open("sample.csv", "r") as f:
    #     csv_reader = csv.reader(f)
        
        # print([row for row in csv_reader])
    
    with open("sample.csv", "r") as f:
        # reads as dict object
        csv_reader = csv.DictReader(f)
        
        print([row for row in csv_reader])
    
    # Instead of handling csv data manually using csv module
    # using pandas we can read data in tabular format for more concise handling
    import pandas as pd
    
    data = pd.read_csv("sample.csv")
    print(data)
    
    
    
# csv_operations()
    