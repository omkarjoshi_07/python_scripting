
from array import array
from collections import Counter
"""
In Python, lists are a versatile and fundamental 
data structure used to store collections of elements. 
Lists are ordered, mutable (modifiable), and can contain 
elements of different data types, including integers, floats, strings, 
and even other lists. They are one of the most commonly used data structures
in Python due to their flexibility and ease of use
"""

def function_1():
    
    # empty list
    numbers_1 = [] 
    # another way of creating empty list
    numbers_2 = list() 

    numbers_2 = list([1, 12, 3, 4, 66, 77])
    print(f"number_2 = {numbers_2} type = {type(numbers_2)}")
    
    """
    method chaining
    This first method call makes a shallow copy of the list
    This second method call appends the value 7 to the copied list returned 
    by numbers_2.copy()
    The append() method modifies the list in place and returns None, 
    thats why None value will be assigned to copied variable
    """
    
    copied = numbers_2.copy().append(7)
    print(f"copied = {copied}, type = {type(copied)}")
    
    copied = numbers_2.copy()
    copied.append(1000)
    print(f"copied = {copied}, type = {type(copied)}")
   
    """
    In Python, when you assign one variable to another
    like numbers_1 = numbers_2, you are essentially creating a 
    new reference to the same object in memory. Therefore, 
    any changes made to numbers_1 will also be reflected in 
    numbers_2, and vice versa.
    """
    numbers_1 = numbers_2
    numbers_1.clear()
    # above statement will clear numbers_2 also
    print(f"numbers2 = {numbers_2}")
    
    """
    In Below code, copy() method is bascially creating new copy of numbers_2
    and reference of the same is getting stored in the numbers_1. 
    Therefore, any changes made in numbers_1 will not reflect in numbers_2
    """
    numbers_2 = list([1, 12, 3, 4, 66, 77])
    numbers_1 = numbers_2.copy()
    numbers_1.clear()
    print(f"numbers2 = {numbers_2}")
   
    # This creates list with 5 0's
    print([15, 55] * 5)
    
    # list from array
    array_list = list(array("i", [12, 3, 56]))
    print(f"array_list = {array_list}, {type(array_list)}")

    # In below code, at index -1 i.e.(234, 56, 67) is of type tuple
    numbers6 = [[12, 34, 56], (234, 56, 67)]
    
    # Error in below code. Tuples are immutable
    # numbers6[1][0] = 0
    
    """numbers6[1] has reference of (234, 56, 67). Bascially here we are
    assigning 100 to that particular location. Thats why at 1st location
    100 value is getting stored. Remember we are not modifying tuple here, 
    we are simply replacing reference of tuple with 100
    (Please refer PPT for memeory allocation of lists for more information)
    """
    numbers6[1] = 100
    print(f"numbers6 = {numbers6}, {type(numbers6[1])}")

function_1()



def list_operation_1():
    numbers = [10, 20, 30, 40, 50, 50]

    #append
    numbers.append(60)

    # remove last value
    popped_value = numbers.pop()
    print(f"popped_value = {popped_value}")
    print(numbers)
    
    numbers.extend([45, 54, 100])
    
    # count of 50 in list
    counter = Counter(numbers).get(50)
    print(f"count of 50: {counter}")
    print(numbers)
    
    list_of_lists = [['4', '8'], ['4', '2', '28'], ['1', '12'], ['3', '6', '2']]
    
    # nested list comprehension
    result = [int(i) for sublist in list_of_lists for i in sublist]  
    print(f"result = {result}")
    
    nested_list = [[12, 34, 55], [33, 3 ,3]]
    flattened_list = [i for ele in nested_list for i in ele]
    print(f"flattened_list = {flattened_list}")

# list_operation_1()


def list_operation_2():
    numbers = [10, 20, 30, 40, 50]
    print(numbers)

    # positive indexing
    print(f"numbers[0] = {numbers[0]}")
    print(f"numbers[1] = {numbers[1]}")
    
    print(numbers[1:-2:2])
    
    # negative indexing
    print(f"numbers[-1] = {numbers[-1]}")
    print(f"numbers[-2] = {numbers[-2]}")
   

    positions = range(len(numbers))
    for i in positions:
        print(f"numbers[{-(i+1)}] = {numbers[-(i+1)]}")
        
# list_operation_2()

def list_operation_3():
    numbers = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
    print(f"numbers from 2 to 6 = {numbers[2:7]}")

    print(f"1st 5 elements from numbers = {numbers[0:6]}")


    print(f"numbers from 1 to 7 with 2 step = {numbers[1:7:2]}")

    print(f"numbers from even index = {numbers[::2]}")

    print(f"numbers from odd index = {numbers[1::2]}")
    
# list_operation_3()