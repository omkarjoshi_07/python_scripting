# import numpy library
import numpy as np
import time

def func1():

    # array
    # np.array() function creates an object of nd.array() class
    # numbers_2 = np.array(numbers_1) # ok
    arr1 = np.array([20, 40, 54, 40])
    
    # here ndarray is object which is storing int64 value
    # arr1 = np.array([10, 209, 45, 60])
    print(f"arr1 : {arr1}")
    print(f"type : {type(arr1)}")
    print(f"Array elements data type : {arr1.dtype}")
    print(f"Array ele size : {arr1.itemsize}")
    print(f"Total no of ele : {arr1.size}")
    print(f"total memory consumption : {arr1.itemsize * arr1.size}")
    
    # explicitly specifying data type
    # We have control over bytes allocated to numpy arrray using 'dtype=np.float32' like wise.
    arr2 = np.array([234, 56 ,67], dtype=np.float16)
    print(f"arr2 : {arr2}, type: {arr2.dtype}")
    print(f"arr2 ele size : {arr2.itemsize}")
    print(f"Total memory taken by arr2 : {arr2.itemsize * arr2.size} bytes")
    
# func1()

def func2():
    arr2 = np.array([[10, 20, 40, 54],
                      [3, 5, 7, 9]])
     # ndim gives no of dimensions
    print(f"dimensions : {arr2.ndim}")
    # shape gives no of rows and columns like -> (2, 4) -> means 2 rows and 4 columns
    print(f"How many ele in {arr2.ndim} dimensions or no. of rows : {arr2.shape}")
     
    # Note as we have created 2 dimensional array above, memory will get allocated contigously
    
    # In below statement, no of items is different in each dimension i.e. there will be 2 rows and 3 columns -> 1st row
    # and 4 columns in 2nd row which will give 'VisibleDeprecationWarning' (Deprecation warning) so we need
    # to specify the dtype=object in future numpy versions
    arr_1 = np.array([[1, 2, 4], [5, 6, 7, 9]], dtype=object)
    print(f"arr_1 : {arr_1}, type of arr_1 : {type(arr_1)}")
    
    
# func2()

def func3():
    arr1 = np.array([1, 2, 4, 6, 9, 10])
    # In below statement, , 3 -> 2*3 -> 6 = no of ele in arr1
    # Here we should specify no of rows and no of columns that should be equal to total no. of ele in array
    arr2 = arr1.reshape(2,3)
    print(arr2)
    print(f"dimensions : {arr2.ndim}")
    print(f"How many ele in {arr2.ndim} dimensions or no. of rows : {arr2.shape}")
    print('*' * 50)

    # arr1.reshape(2, 2) doest not changes core arr1 in memory rather it will return new array so we
    # have to store it into some variable
    arr3 = arr1.reshape(3, 2)
    print(arr3)
    print(f"dimensions : {arr3.ndim}")
    print(f"How many ele in {arr3.ndim} dimensions or no. of rows : {arr3.shape}")
    print('*' * 50)


    
# func3()


def func4():
    # generate arrays
    nums = np.arange(1, 10, 2)
    print(nums)
    
    # generate zeros and ones
    zeros, ones = np.zeros(4), np.ones(4)
    print(zeros, ones)
    
    # broadcasting
    multiplication = nums * 2
    print(multiplication)
    
    # filtering
    filt = nums[nums < 5]
    print(filt)
    
    # Get index at which value < 6
    index_loc = np.where(nums < 6)
    print(nums.take(index_loc))
    
    
# func4()


def matrix_transpose():
    a = np.random.rand(10_000, 20_000)
    
    # It takes 1600 MB to store that much data
    print(f'Matrix `a` takes up {a.nbytes / 10**6} MB') 
    
    start_time = time.time()
    
    # It takes merely nanoseconds to transpose 1600 MB of data
    # transpose() function doesn't actually modify the data in the array. 
    # Instead, it returns a view of the array with its dimensions permuted according to the specified order.
    b = a.transpose()
    end_time = time.time()
    print("Elapsed time:", end_time - start_time, "seconds")
    
    
# matrix_transpose()

def transpose(a):
    """A little known feature of NumPy is the numpy.stride_tricks module 
    that allows you to modify the strides attribute directly
    """
    from numpy.lib.stride_tricks import as_strided
    
    #  It takes an array a and returns a view of it with its shape and strides attributes reversed.
    return as_strided(a, shape=a.shape[::-1], strides=a.strides[::-1])

def strides():
    """
    Strides means how data can be accessed in memory
    The strides attribute contains for each dimension, 
    the number of bytes (not array indexes) we have to skip over to get to the
    next element along that dimension.
    """
    
    # (64, 8) -> It means to move to next element along the row we need to move 64 bytes in the memory
    # and to move next element along the column we need to move 4 bytes in the memory
    print(np.zeros((4, 8)).strides)
    
    
    """
    So now we know the mystery behind the speed of transpose(). 
    NumPy can avoid copying any data by just modifying the strides of the array
    When you call a.transpose(), NumPy doesn't actually modify the data in the array. 
    Instead, it returns a view of the array a with its dimensions permuted.
    In this case, the rows become columns and vice versa.
    """
    a = np.random.rand(10_000, 20_000)
    b = a.transpose()

    print(a.strides)  # (160000, 8)
    print(b.strides)  # (8, 160000)
    
    
    # Testing the function on a small matrix
    a = np.array([[1, 2, 3],
                  [4, 5, 6]])
    print('Before transpose:')
    print(a)
    print('After transpose:')
    print(transpose(a))
    

strides()

