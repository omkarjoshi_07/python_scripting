# -*- coding: utf-8 -*-
"""
Created on Wed Feb 14 14:50:58 2024

@author: DELL
"""
from array import array
import numpy as np

# arrays
# An array is a collection of items stored at contiguous memory locations. 
# The idea is to store multiple items of the same type together. 
# Arrays can be created by using array module


# Create an array of integers
int_array = array('i', [1, 2, 3, 4, 5])

# Access elements by index
print("First element:", int_array[0])  # Output: 1
print("Last element:", int_array[-1])   # Output: 5

# Modify elements
int_array[0] = 10

# Iterate over the array
print("Array elements:")
for num in int_array:
    print(num)

# Append elements
int_array.append(6)

# Insert elements
int_array.insert(2, 7)  # Insert 7 at index 2

# Remove elements
int_array.remove(4)  # Remove the element 4


# Convert array to list
int_list = list(int_array)
print("Array converted to list:", int_list)

nums = [12, 34, 56]
arr3 = array("i", nums)
print(f"arr3 = {arr3}, type = {type(arr3)}")

# Create a 1D array from a Python list
arr1 = np.array([1, 2, 3, 4, 5])

# Create a 2D array (matrix) from a nested Python list
arr2 = np.array([[1, 2, 3], [4, 5, 6]])

# Create a NumPy array with a specified data type
arr3 = np.array([1, 2, 3], dtype=np.float64)
