# import json module
import json
from datetime import datetime

def function_1():
    
    # data is always stored in key-value pairs in json
    json_string = '''{
        "persons":
        [
                {
                    "name":"Alex",
                    "Email":"Alex@hotmail.com",
                    "Phone":9099999009,
                    "Salary": null
                },
                {
                    "name":"Jhon",
                    "Email":"Jhon@rediffmail.com",
                    "Phone": 2349535435
                }
        ]
       }
    '''
    
    json_string2 = '''
           [{
                    "name":"Alex",
                    "Email":"Alex@hotmail.com",
                    "Phone":9099999009
                },
                {
                    "name":"Jhon",
                    "Email":"Jhon@rediffmail.com",
                    "Phone": 2349535435
                }]
            
        
    '''    
    # DeSerialize Python dictionary to JSON string
    # JSON allows you to deserialize JSON strings into Python objects. 
    # The json module provides functions such as json.loads() to deserialize JSON strings into Python objects.
    data2 = json.loads(json_string)
    print(data2)
    # print([(k, v) for k,v in data.items()])

    print([(k, v) for person in data2['persons'] for (k,v) in person.items()])


# function_1()


def function_2():
    """
    Convert json string to python object
    """
    json_string = '''{
        "persons":
        [
                {
                    "name":"Alex",
                    "Email":"Alex@hotmail.com",
                    "Phone":9099999009,
                    "Salary": 65000.00
                },
                {
                    "name":"Jhon",
                    "Email":"Jhon@rediffmail.com",
                    "Phone": 2349535435,
                    "Salary": 44000.00
                }
        ]
       }
    '''
    data = json.loads(json_string)

    print([(k, v) for person in data['persons'] for k, v in person.items() if 'Salary' != k])
    
# function_2()


def function_3():
    """
    Comvert python object to json string
    Serilization:
        JSON allows you to serialize Python objects into a string 
        representation 

    """
    json_string = '''{
        "persons":
        [
                {
                    "Name":"Alex",
                    "Email":"Alex@hotmail.com",
                    "Phone":9099999009,
                    "Salary": 65000.00
                },
                {
                    "Name":"Jhon",
                    "Email":"Jhon@rediffmail.com",
                    "Phone": 2349535435,
                    "Salary": 44000.00
                }
        ]
       }
    '''
    data = json.loads(json_string)
    
    # indent parameter below specifies ow many spaces to use for indentation
    # sort_keys ensures keys in the json to be sorted alphabetically
    converted_to_json = json.dumps(data, indent=2, sort_keys=True)
    print(converted_to_json)
    
# function_3()

def function_4():
    # This line opens the file "states.json" in the current directory for reading
    # ('r' mode) using a context manager. The file handle is assigned to 
    # the variable f. The with statement ensures that the file is properly 
    # closed after reading, even if an exception occurs.
    with open('./states.json') as f:
        data = json.loads(f.read())
    
    # print(data)
    
    # remove area_codes 
    for state in data['states']:
        del state['area_codes']
    print(data)
    with open('states_with_abbr.json', 'w') as f:
        json.dump(data, f, indent=2)
    
    if isinstance(data, dict):
        print("Data is converted to dict..")
        
    
# function_4()


def datetime_handler(obj):
    if isinstance(obj, datetime):
        print("is instance")
        return obj.isoformat()



def function_5():
    
    """
    The default behavior of the json module cannot handle datetime objects.
    But you can define a custom function to process such objects.
    """
    
    # JSON does not have a native representation for Python's datetime or
    # custom objects. Therefore, when you try to serialize such objects, 
    # you may encounter errors. However, the json module provides ways to  handle them.
    # Below line gives error
    # json.dumps(datetime.now())
    
    now = datetime.now()
    print(now, type(now))
    
    # The default handler is invoked when object passed to dumps()
    # is not serializable. e.g. in below case, it will return serialized date time object
    json_str = json.dumps(now, default=datetime_handler)
    print(json_str)
    
    
function_5()

def function_6():
    """
    JSONDecodeError is an exception raised by the json module when there's an issue with decoding a JSON string. 
    """
    malformed_json = '{"name": "John", "age": }'  # Notice the missing age value
    try:
        data = json.loads(malformed_json)
    except json.JSONDecodeError as e:
        print(f"Error message: {e.msg}")
        print(f"Error position (line, column): {e.lineno}, {e.colno}")
        

# function_6()


def live_exchange_rate_using_json():
    """
    This function makes fetches live exchange rates by api call

    """
    # import requests library to make api call
    import requests
    
    # load configurations from config.json into python json object
    with open("config.json", "r") as f:
        # json.loads() is used to parse str like objects. It will give error
        # while parsing file like object
        config_data = json.load(f)
    
    # print(config_data)
    
    # get required credentials
    api_url = config_data["api_url"]
    api_key = config_data["api_key"]
    req_url = f"{api_url}?app_id={api_key}"
    response = requests.get(req_url)
    
    if response.status_code // 100 == 2:
        try:
        # Parse the JSON response
            data = response.json()
            
            # Print exchange rates for USD
            exchange_rates = data['rates']
            
            # get required currencies
            currencies_of_interest = ['CAD', 'INR', 'GBP', 'EUR']
            exchange_rates2 = {currency: data['rates'][currency] for currency in currencies_of_interest}
            # print(exchange_rates2)
            print([f"USD to {currency}: {rate}" for currency, rate in exchange_rates2.items()])

        except ValueError as e:
            print(f'Error parsing JSON: {e}')
            print(f'Response Content: {response.text}')
    else:
        print(f'Error: {response.status_code} : {response.text}')
    

# live_exchange_rate_using_json()
