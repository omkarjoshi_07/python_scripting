# import beautiful soup
# beautiful soup is specifically used for static html
from bs4 import BeautifulSoup


def function1():
    html = """
        <html>
            <body>
                <h1>this is h1</h1>  
            </body>
        </html>
    """

    # print(html)

    # create a soup
    soup = BeautifulSoup(html, "html.parser")

    # find the h1 element
    h1 = soup.find('h1')
    # print(h1)

    # extract data inside h1
    print(h1.text)


# function1()


def function2():
    # file = open('templates/index.html', 'r')
    # contents = file.read()
    # file.close()

    with open('./templates/index.html') as file:
        contents = file.read()

        soup = BeautifulSoup(contents, "html.parser")

        # find element named div
        # div = soup.find("div")
        # print(div.text)

        # find all elements named div
        divs = soup.find_all('div')
        
        # 1st occurrence of div element
        # print(soup.find('div'))
        
        
        # Below code gives result set
        """
        A ResultSet is essentially a list-like collection of BeautifulSoup 
        Tag objects. When you call find_all() to search for elements in an 
        HTML document, BeautifulSoup returns a ResultSet containing all 
        the elements that match the specified criteria. Each element in the 
        ResultSet is represented as a Tag object.

        You can iterate over a ResultSet like you would with a list, 
        accessing individual Tag objects to extract information such as 
        tag names, attributes, and text content.
        """
        # print(type(divs))
        print(divs[1].text)
        
        # parent elements
        # print(divs[1].parent)
       


# function2()



def function3():
    """
    Find temperature from html file from daily forecast website
    """
    
    info = []
    with open('templates/temp.html', 'r') as file:
        html = file.read()

        soup = BeautifulSoup(html, 'html.parser')

        # find container div
        div = soup.find('div', {"class": "DailyForecast--DisclosureList--350ZO"})

        # find all details elements
        details_elements = div.find_all('details', {"data-track-string": "detailsExpand"})

        for details in details_elements:
            # search for an <h2> tag with the attribute data-testid set to "daypartName" within the context of the details element.
            day = details.find("h2", {"data-testid": "daypartName"})

            # find span tag with data-testid set to TemperatureValue
            # 1st parameter is the tag and 2nd argument in {} are its attributes
            max_temp = details.find("span", {"data-testid": "TemperatureValue"})
            # print(max_temp.text[0:2])
            
            
            # find min_temp
            min_temp = details.find("span", {"data-testid": "TemperatureValue", "class": "DetailsSummary--lowTempValue--1DlJK"})

            # condition
            condition = details.find("span", {"class": "DetailsSummary--extendedData--aaFeV"})
            # print(condition.text)
            
            # print(f"day: {day.text}, max: {max_temp.text.replace('°', '')}, min: {min_temp.text.replace('°', '')}, condition: {condition.text}")
            info.append({
                "day": day.text,
                "max_temp": max_temp.text[0:2],
                "min_temp": min_temp.text[0:2],
                "condition": condition.text
            })

    print(info)
    
    # store details into csv file
    with open('info.csv', 'w', encoding='utf-8') as file:
        file.write("day,max_temp_DegC,min_temp_DegC,condition\n")
        for data in info:
            file.write(f"{data['day']},{data['max_temp']},{data['min_temp']},{data['condition']}\n")


# function3()


def function4():
    import pandas as pd

    df = pd.read_csv('./info.csv')
    print(df)


function4()
