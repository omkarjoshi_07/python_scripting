# import numpy & pandas module
import numpy as np
import pandas as pd

def pandas_basic():
    # dtype = int64
    series1 = pd.Series([12, 43, 67, 89])
    print(f"series1 : {series1}, type : {type(series1)}")

    # dtype = object
    ser2 = pd.Series([12, "xyz", 234,67])
    print(f"ser2 : \n{ser2}, type : {type(ser2)}")
    
    # seperating indexes and values
    print(f"Indexes : {series1.index}")
    print(f"Values : {series1.values}")
    print(f"series[0] : {series1[0]}")

    # below statement will give keyerror as -a index is not avalable inside the series
    # print(f"series[-1] : {series1[-1]}")
    
    cars = ["i24", "i10", "alto", "Nano"]
    avg = [15, 18, 20, 15]

    # ser1 = pd.Series(cars)
    # print(f"ser1 : {ser1}")


    ser1 = pd.Series(cars, index=avg)
    print(f"cars and Avg : {ser1}")
    
    dict1 = {'alto' : 15, 'nano' : 20}
    # In below statement, we have passed dict as data to series where keys in the dict will be assigned as indexes
    ser3 = pd.Series(dict1)
    print(f"ser3 : {ser3}")
    print(f"dtype  {ser1.dtype}")
    print(f"ndim : {ser1.ndim}")
    print(f"shape : {ser1.shape}")
    print(f"size : {ser1.size}")



# pandas_basic()


def pandas_dataframe():
    d = {'col1': [1, 2], 'col2': [3, 4]}
    df = pd.DataFrame(data=d)
    
    print(df.head())
    
    df = pd.DataFrame(data=d, dtype=np.int8)
    print(df.dtypes)
    
    d = {'col1': [0, 1, 2, 3], 'col2': pd.Series([2, 3], index=[2, 3])}
    df2 = pd.DataFrame(data=d, index=[0, 1, 2, 3])
    print(df2)
    
    data = {'Name': ['Alice', 'Bob', 'Charlie', 'Dölek', 'Élise'],
        'Age': [25, 30, 35, 40, 45]}

    df3 = pd.DataFrame(data)
    
    # Save the DataFrame to a CSV file with 'utf-8' encoding
    # df3.to_csv('example_utf8.csv', index=False, encoding='utf-8')
    
    # While reading above csv file into pandas dataframe, we have ensure that
    # to use "utf-8" as encoding option to correctly handle these special characters
    
    df4 = pd.read_csv("example_utf8.csv", encoding="utf-8")
    print("\n", df4.head())
    
    

# pandas_dataframe()


def df_operations():
    url = "https://raw.githubusercontent.com/pandas-dev/pandas/master/doc/data/titanic.csv"
    titanic = pd.read_csv(url, index_col='Name')
    
    # print the first 5 lines of the dataframe
    # print(titanic.head(5))
    
    # print summary statistics for each column
    # print(titanic.describe())
    
    # find the average age of those who survived or didn’t survive
    print(titanic.groupby("Survived")["Age"].mean())    
    
    #  plot corresponding histograms
    # titanic.hist(column='Age', by='Survived', bins=25);
    
    
    
df_operations()