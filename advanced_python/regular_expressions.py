# A re module specifies a set of strings that matches it; 
# the functions in this module let you check if a particular 
# string matches a given regular expression
import re
import pandas as pd


"""
Pls refer ppt for character classes and sets in regex
Following are some functions in regex and their functionality:
    re.findall(): Finds all occurrences of words containing the letter "o".
    re.search(): Searches for the first occurrence of the word "dog".
    re.match(): Matches the pattern "The" at the beginning of the string.
    re.split(): Splits the text into sentences based on the period.
    re.sub(): Replaces all occurrences of "dog" with "cat" in the text.
"""

def func1():
    text = "The quick brown fox jumps over the lazy dog. The dog barks."

    # 1. re.findall(): Find all occurrences of words containing "o"
    # \b-matches start or end of word, \w* - one or more occurrences of letters/characters
    words_with_o = re.findall(r'\b\w*o\w*\b', text)
    print(words_with_o)
    
    # 2. re.search(): Search for the first occurrence of "dog"
    match = re.search(r'\bdog\b', text)
    if match:
        print("Found 'dog' at position:", match.start())
    else:
        print("No match found.")
        
    # 4. re.split(): Split the text into sentences
    sentences = re.split(r'\.', text)
    print(sentences)
    
    # 5. re.sub(): Replace "dog" with "cat"
    new_text = re.sub(r'\bdog\b', 'cat', text)
    print(new_text)

# func1()



def find_mobile_pattern():
    indian_mobile_pattern = r'\b\+91[789]\d{9}\b'

    numbers = [
        "+917890123456",
        "+917654321098",
        "+919876543210",
        "+911234567890",  # Incorrect, starts with +91 but the next digit is not 7, 8, or 9
        "+9176543210",    # Incorrect, does not have 10 digits after +91
        "+9187654321090"  # Incorrect, has more than 10 digits after +91
    ]
    
    for number in numbers:
        if re.match(indian_mobile_pattern, number):
            print(f"{number} is a valid Indian mobile number.")
        else:
            print(f"{number} is NOT a valid Indian mobile number.")
    
    """
    \b: Matches a word boundary to ensure that the mobile number is separated from other characters by word boundaries.
    \+91: Matches "+91", the country code for India.
    [789]: Matches a single digit that can be 7, 8, or 9, indicating the next digit after "+91".
    \d{9}: Matches exactly 9 digits after the country code and the initial digit, 
        ensuring a total of 10 digits in the mobile number.
    \b: Matches a word boundary at the end of the mobile number to ensure it is properly terminated.
    """
        
    
# find_mobile_pattern()


def find_domain(row):
    # pattern for matching
    # In below code, we have created groups with () for each pattern
    # e.g. (https?://) - http word must be there and 's' letter is optional(?).
    # So this is one group and so on..
    pattern = r'(https?://)?(www\.)?([\w-]+(\.[a-z]{2,}){1,2})'
    
    # searches pattern in the data frame column and returns match object
    result = re.search(pattern, row['Description'])
#     display(re.search(pattern, row['Description']).group('domain'))
    #  If a match is found, it extracts and returns the third group of the match, which corresponds to the domain name in the regular expression pattern.
    return result.group(3)

def func2():
    # extracting domain name from description column
    # e.g. https://bestbuy.com -> output should be- bestbuy.com
    data = data = {
    "Description": ["The official homepage URL for 'WAVE BUSINESS' is https://business.wavebroadband.com/",
                    "Check out the new offers on electronics at https://www.bestbuy.com/",
                    "Get your favorite coffee at https://www.starbucks.com/ today.",
                    "Enjoy delicious pizzas at https://www.dominos.com/.",
                    "Shop for trendy clothes at https://www.hm.com/",
                    "Upgrade your phone at https://www.apple.com/.",
                    "Explore the latest books at https://www.barnesandnoble.com/.",
                    "Discover exciting games at https://www.gamestop.com/.",
                    "Treat yourself to a movie night at https://www.amctheatres.com/.",
                    "Visit the zoo for a fun day out with family at https://www.zoo.com/"],
    "Net Spend": [100, 150, 80, 200, 120, 300, 90, 70, 50, 110],
    "Year": [2023, 2023, 2023, 2023, 2023, 2023, 2023, 2023, 2023, 2023],
    "Category": ["Internet", "Electronics", "Food & Beverage", "Food & Beverage", "Fashion", 
                 "Electronics", "Books", "Games", "Entertainment", "Entertainment"]
}
    df = pd.DataFrame(data)
    
    # using apply method to work on each descrition row for pattern matching
    print(df.apply(find_domain, axis=1))
    
    
func2()


