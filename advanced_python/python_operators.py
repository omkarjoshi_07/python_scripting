#operators
# print(f"10 + 20 = {10+20}")
# print(f"30 - 20 = {30-20}")
print(f"5 /2 = {5 / 2}")
#  / true division gives float result
# print(f"30 / 10 = {30/10}")
# print(f"15/4 = {15/4}")

# // floor div gives int value
print(f"15//4 = {15//4}")
# print(f"10*4 = {10*4}")

#cube of 5 -> 3rd power of 5
print(f"5**3 = {5**3}")

#repeatation operator
#prints 1 10 times
print("- "*10)

# print("* "*5)

# for i in range(1,6):
#     print("* "*5)


#comparison operators
# <, >, <=, >=, ==, !=
print(f"10>5 = {10>5}")
print(f"10<5 = {10<5}")
print(f"10<=5 = {10<=5}")
print(f"10>=5 = {10>=5}")
print(f"5==5 = {5==5}")
print(f"10!=5 = {10!=5}")


#logical operators

print(f"True and False = {True and False}")
print(f"False and True = {False and True}")
print(f"False and False = {False and False}")

print(f"True or True = {True or True}")
print(f"True or False = {True or False}")
print(f"False or True = {False or True}")
print(f"False or False = {False or False}")



