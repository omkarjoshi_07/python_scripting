
def function_1():
    try:
        # this code will be first tried out to save our application from crashing
        str1 = input("Enter a number : ")
        num = int(str1)
        print(f"num = {num}, type = {type(num)}")
    except:
        # used to catch the errors.
        # once the error it won't reach to PVM(Python Virtual Machine which results in non crashing application.
        print(f"Application has received an error")
        

# function_1()


def function_2():
    try:
        num1 = int(input("Enter number 1 : "))
        num2 = int(input("Enter number 2 : "))
        result = num1/num2
    except ValueError:
        print(f"Divide by zero exception.. Please try again..")
        # function_2()
        
    except ZeroDivisionError:
        print("Division by zero error...Denominator cannot be zero")
    except:
        # generic except block
        print("Some other error..")
    else:
        # will get executed when there is no error
        print(f"Result = {result}")


# function_2()

# custom exceptions

# This class must be derived from Exception class
class InvalidAgeError(Exception):
    # def __init__(self):
    #     print("Invalid age entered..")
    pass


def function_3():
    name = input("Enter your name : ")
    age = int(input("Enter your age : "))
    address = input("Enter your address : ")
    
    # excaption can be raised anywhere in the code. It is not necessarily added in the try block
    if age < 25 or age > 60:
        raise InvalidAgeError

    print(f"Name : {name}")
    print(f"Age : {age}")
    print(f"Address : {address}")


try:
    function_3()
    
except InvalidAgeError:
    print("Exception: Invalid age entered..")


def function_4():
    try:
        # used to add the code which may generate an error
        file = open("./test.txt", "r")
    except FileNotFoundError:
        # used to handle FileNotFoundError
        print("file not present on the location")

        # try:
        #     division = 10 / 0
        #     print(f"division = {division}")
        # except ZeroDivisionError:
        #     print("denominator can not be zero")

    except:
        # used to handle any other error than FileNotFoundError
        print("exception has received...")

        # try:
        #     division = 10 / 0
        #     print(f"division = {division}")
        # except ZeroDivisionError:
        #     print("denominator can not be zero")

    else:
        # used to add code which will be called only when
        # there is no exception being raised
        data = file.read()
        print(f"data = {data}")

        try:
            division = 10 / 0
            print(f"division = {division}")
        except ZeroDivisionError:
            print("denominator can not be zero")
        finally:
            print("finally called for division error")

    finally:
        # used to add the code which will be called
        # irrespective of error being raised
        if file != None:
            file.close()


function_4()
