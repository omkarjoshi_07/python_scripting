class Account:
    def __init__(self, balance, type):
        self.__balance = balance
        self.__type = type

    def __str__(self):
        return f"Account Balance : {self.__balance}, Tyep : {self.__type}"


if __name__ == "__main__":
    a1 = Account(500000.00, "Saving")
    print(a1)
