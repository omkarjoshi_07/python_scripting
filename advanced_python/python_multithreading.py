import time

# import threading module
import threading


def calc_sqaure(lst):
    print("Calculating Squares: ")
    for ele in lst:
        # cpu will be idle for 0.2 seconds
        time.sleep(0.2)
        print(f"Square of {ele}: {ele ** 2}")
        


def calc_cube(lst):
    print("Calculating Cubes: ")
    for ele in lst:
        time.sleep(0.2)
        print(f"Cube of {ele}: {ele ** 2}")


def normal_operation():
    # calculate squares and cubes of list elements at the same time
    
    """
    Basically in this case, we are calling squares function 1st and then cubes.
    In each function time.sleep() will cause CPU to be idle for 0.2 seconds.
    Here, squares are caculated 1st and printed, then cubes are calculated
    """
    lst = [1, 4, 5, 15]
    
    t = time.time()
    calc_cube(lst)
    
    calc_cube(lst)
    
    # after checking its output, this takes generally 1.6 seconds to execute 
    # i.e. calculates squares and cubes
    
    print("time required: ", time.time() - t)
    

# normal_operation()


def multi_operaion():
    lst = [1, 4, 5, 15]
    t = time.time()
    """
    In this case, when CPU is idle, then new task will be assigned to it.
    thats why in output we can see, whenever it is idle it will do different task
    """
    
    # below target is what task to be executed and args is tuple or list of parameters
    t1 = threading.Thread(target=calc_sqaure, args=(lst, ))
    t2= threading.Thread(target=calc_cube, args=(lst, ))
    
    # cpu assigns identity to each thread only after thread is started
    # below code will give None as it is not started
    print(t1.ident)
    
    # create two threads to carry out two functionalities 1.Sqaures 2. Cubes
    t1.start()
    t2.start()
    
    print(f"t1 identity: {t1.ident}")
    
    """
    The join() method is called on both threads to ensure that the main 
    thread waits for them to complete before proceeding.
    """
    t1.join()
    t2.join()
    """
    If you don't use the join() method after starting the threads, 
    the main thread will not wait for the threads to complete their execution. 
    This means that the program may terminate before the threads finish their 
    tasks. i.e program may finish execution and print statement below without 
    having completion of calculation sqaures and cubes of all the elements
    """
    
    # it takes only 0.8 seconds to execute
    print("time required: ", time.time() - t)
    
    """
    Output:
        Calculating Squares: 
        Calculating Cubes: 
        Square of 1: 1
        Cube of 1: 1
        Cube of 4: 16
        Square of 4: 16
        Square of 5: 25Cube of 5: 25

        Square of 15: 225
        Cube of 15: 225
        time required:  0.8360087871551514
    """
    

# multi_operaion()

def first_person():
    print("1st person is making order..")
    # will take 2 seconds to order 
    time.sleep(2)
    
    print("1st person has given the order and vacates the space..")
    

def second_person():
    print("2nd person is making order..")
    # will take 2 seconds to order 
    time.sleep(2)
    
    print("2nd person has given the order and vacates the space..")
    
def third_person(lock):
    # acquire the lock
    lock.acquire()
    
    print("3rd person is making order..")
    # will take 2 seconds to order 
    time.sleep(2)
    
    print("3rd person has given the order and vacates the space..")
    
    # release the lock
    lock.release()
    

def fourth_person(lock):
    # acquire the lock
    lock.acquire()
    print("4th person is making order..")
    
    
    # will take 2 seconds to order 
    time.sleep(2)
    
    print("4th person has given the order and vacates the space..")
    
    # release the lock
    lock.release()



def thread_synchronization():
    # this is used for protecting data across threads
    # mutual exclusion -> This means if one thread is holding one resource other thread cannot access it
    
    """
    Consider a scenario where one person can make order of pizza at a time
    But in below case, thats not the case.. when CPU gets idle while executing one
    thread, other thread will get executed
    """
    
    # now create 2 threads and let them run
    # t1 = threading.Thread(target=first_person)
    # t2= threading.Thread(target=second_person)
    
    # t1.start()
    # t2.start()
    
    # t1.join()
    # t2.join()
    
    # Now to execute one thread at a time -> a condition where only one person
    # can order pizza at a time, we can use thread synchronization
    
    # create the global lock and pass whichever threads we want to pass
    lock = threading.Lock()
    
    """
    In below code, t4 can not enter when t3 is executing..only after t3 releases
    its lock, t4 will start executing and vice a versa
    """
    t3 = threading.Thread(target=third_person, args=(lock, ))
    t4 = threading.Thread(target=fourth_person, args=(lock, ))
    
    t3.start()
    t4.start()
    
    t3.join()
    t4.join()
    
    print("Finished with execution..")
    
    
    

# thread_synchronization()
    
    
        

    
        


