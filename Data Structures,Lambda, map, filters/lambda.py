"""
Inline functions
Executed on same line
Take as many parameters or at least 1 parameters
No separate stackframe created fro it
"""

def add(n1, n2):
    return n1 + n2


# print(f"Addition is {add(2,3)}")

def cal_modulus(num1, num2):
    return num1 % num2


# print(f"{cal_modulus(12, 2)} ")
if cal_modulus(12, 4) == 0:
    print("modulus is true")
else:
    print("modulus is false")


def square(n1, n2):
    return n1 * n2


# print(f"Square = {square(2,3)}")

# lambda -> unnamed/anonymous function
add_lambda = lambda n1, n2: n1 + n2
# <variable> = lambda <parameter1>, <parameter2>: <parameter1 + <parameter>2
print(f"Addition = {add_lambda}, type = {type(add_lambda)}, \n value: {add_lambda(15, 30)}")

# lambda function call
addition = add_lambda(10,20)
# print(f"Addition = {addition}")

annual_salary = lambda sal: sal * 12
# print(f"Annual Salary = {annual_salary(15000.00)} per anum")

# dummy lambda
dummy_lambda = lambda p1 : print(f"p1 = {p1}")
# dummy_lambda = lambda p1 : p1*20
# print(f"dummy lambda = {dummy_lambda(20)}")

# Note: We cannot define lambda withhin lambda but we can call lambda within lambda

# nested lambda
add_bonus = lambda bonus: annual_salary(15000.00) + bonus
print(f"Annual salary with bonus = {add_bonus(15000)}")

# lambda function without parameters
test_lambda_1 = lambda: print("this is lambda function without taking parameter")
test_lambda_1()

test_lambda_2 = lambda param=20: print("this is lambda function with default argument")
test_lambda_2()

# lambda within function
def multiplier(num):
    # returning lambda function
    return lambda x: x * num


var = multiplier(10)

print(var(50))

# Define a list of dictionaries representing different mathematical operations
operations = [
    {'name': 'addition', 'symbol': '+', 'function': lambda x, y: x + y},
    {'name': 'subtraction', 'symbol': '-', 'function': lambda x, y: x - y},
    {'name': 'multiplication', 'symbol': '*', 'function': lambda x, y: x * y},
    {'name': 'division', 'symbol': '/', 'function': lambda x, y: x / y if y != 0 else 'undefined'}
]

# Perform each operation with given operands
operands = (10, 2)
results = {}
for op in operations:
    results[op['name']] = op['function'](*operands)

# Print the results
for op_name, result in results.items():
    print(f"{operands[0]} {op_name} {operands[1]} = {result}")


