"""
Python tuples are a type of data structure that is very similar to lists. 
The main difference between the two is that tuples are immutable, 
meaning they cannot be changed once they are created. This makes them 
ideal for storing data that should not be modified
1. count()
2. index()
3. any()
4. min()
5. max()
"""

def function_1():
    #empty lists
    list_1 = []
    print(f"list_1 = {list_1}, type = {type(list_1)}")

    list_2 = list()
    print(f"list_2 = {list_2}, type = {type(list_2)}")

    #empty tuples
    tuple_1 = ()
    print(f"tuple_1 = {tuple_1}, type = {type(tuple_1)}")

    tuple_2 = tuple()
    print(f"tuple_2 = {tuple_2}, type = {type(tuple_2)}")

    # 10 as int in round bracket
    tuple_1 = (10)
    print(f"tuple_1 = {tuple_1}, type = {type(tuple_1)}")
    
    
    # tuple
    tuple_3 = (10,)
    print(f"tuple_3 = {tuple_3}, type = {type(tuple_3)}")
    
    # doesn't modify existing tuple, but creates new one with additional element
    tuple4 = (1, 3, 4) + (10,)
    print(tuple4)
    
    
function_1()

def function_2():
    numbers_1 = [10, 20, 30]
    print(f"numbers_1 = {numbers_1}")
    numbers_1.append(50)
    print(f"numbers_1 = {numbers_1}")


    #tuple is immutable
    tuple_1 = (10, 20, 30, 40)
    
    # we can't modify tuple once created
    # tuple_1.append(90) # not ok

    print(f"tuple_1 = {tuple_1[0:3:2]}")
    
    
    # numbers = (10, 20)
    numbers = 10, 20
    print(f"numbers = {numbers}, type = {type(numbers)}")
    print(f"numbers[0] = {numbers[0]}")
    print(f"numbers[1] = {numbers[1]}")

    n1 = 100
    n2 = 200
    n1, n2 = 10, 20  # tuple behind the scene -> (n1, n2) = (10, 20) and can be decoupled.
    print(f"n1 = {n1}, type = {type(n1)}")
    print(f"type of n1 and n2 = {type((n1, n2))}")
    


# function_2()