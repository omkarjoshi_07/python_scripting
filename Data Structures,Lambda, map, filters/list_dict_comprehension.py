
def function_1():
    """
    list comprehension
    """
    numbers = [1, 4, 5, 2, 3, 7, 8, 9]
    squares = [n ** 2 for n in numbers]
    print(f"squares = {squares}")
    
    numbers = [1, 4, 5, 2, 3, 7, 8, 9]
   
    even_nos = [n for n in numbers if n % 2 == 0]
    print(f"even nos = {even_nos}")
    
    # list comprehension
    numbers_squares = [(n, n ** 2) for n in numbers]
    print(f"numbers and squares using list comprehension= {numbers_squares}")
    
    """
    Nested list comprehension: 
            Nested comprehensions allow us to generate complex, 
            multi-dimensional lists, sets, or dictionaries efficiently.
    """
    # Creating a 3x3 multi-dimensional list filled with zeros
    matrix = [[0 for _ in range(3)] for _ in range(3)]
    print(matrix)
    
    # Creating a set of tuples representing coordinates in a 3x3 grid
    coordinates = {(x, y) for x in range(3) for y in range(3)}
    print(coordinates)




# function_1()

def function_2():
    # Example: Create a dictionary with conditional logic
    numbers = [1, 2, 3, 4, 5]
    even_odd_dict = {num: 'even' if num % 2 == 0 else 'odd' for num in numbers}
    print(even_odd_dict)
    
    # Example: Convert keys or values to uppercase
    my_dict = {'a': 'apple', 'b': 'banana', 'c': 'cherry'}
    uppercase_keys = {key.upper(): value for key, value in my_dict.items()}
    print(uppercase_keys)
    
    
    
# function_2()

def transformations_using_comprehension():
    # nested dict comprehension
    # Creating a multi-dimensional dictionary representing a chessboard
    chessboard = {(row, col): ('white' if (row + col) % 2 == 0 else 'black')
              for row in range(1, 9)
              for col in range(1, 9)}
    print(chessboard, end=("\n\n"))

    # Original matrix with row indices as keys
    matrix = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
    ]
    
    # Creating Dictionary with row indices as keys using list comprehension
    matrix_dict = {i: matrix[i] for i in range(len(matrix))}
    print(matrix_dict, end=("\n\n"))

    # Conditional application using lambda within a comprehension
    results = [(lambda x: x**2 if x%2 == 0 else x**3)(num) for num in range(1, 10)]
    
    print(results, end=("\n\n"))
    
    """
    For memory efficient operations, using built-in functions, numpy arrays, etc
    is  vital to find the most computationally efficient solution
    """
    # Using Comprehension
    squares = [x*x for x in range(100)]
    print(squares, end=("\n\n"))
    
    # Using map may offer better performance 
    squares = list(map(lambda x: x*x, range(100)))
    print(squares)
    
# transformations_using_comprehension()
