class Person:
    def __init__(self, name):
        self.__name = name

    def __str__(self):
        return f"Person Name : {self.__name}"


if __name__ =="__main__":
    p1 = Person("Dilip")
    print(p1)