"""
A set is an unordered collection of unique elements.
 Sets are mutable, meaning you can add or remove elements after creation, 
 but they do not support indexing or slicing like lists do. 
 Sets are commonly used for tasks where you need to store unique elements and
 perform set operations such as intersection, union, and difference
 1. add()
 2. remove
 3. pop()
 4. copy()
 5. union()
 6. intersection()
"""

def function_1():
    # to create empty set
    set_2 = set()
    print(f"set_2 = {set_2}, type = {type(set_2)}")

    set_1 = {10, 20, 30, 40, 50, 10, 20, 30, 40, 50, 10, 20, 30, 40, 50}
    set_1.add('java')
    print(set_1)
    
    set3 = {1, 2, 3, 4, 5}
    set4 = {4, 5, 6, 7, 8}

    # Create a new set by performing union and then intersection
    result = set3.union(set4).intersection({3, 6, 9})
    print(result)
    
    # modify existing set with new item
    set_1 |= {1000}
    print(f"set_1 = {set_1}, type = {type(set_1)}")
    
    # remove duplicacy from list
    cities = ["Pune", "Mumbai", "Delhi", "Kolhapur", "Goa", "Pune", "Mumbai"]
    print(f"Unique cities = {set(cities)}")
    
    # tuple cannot be passed to set to extract the unique elements.
    mobile_brands = ("samsung", "Xiomi", "Oppo", "Vivo", "Samsung", "vivo")
    print(f"Unique brands = {set(mobile_brands)}")


function_1()



"""
A built-in data structure that stores key-value pairs. 
It is an unordered collection of items where each item is represented as a 
key-value pair. Dictionaries are mutable, meaning they can be modified after creation,
 and they are often used to represent mappings between objects
"""

def function_2():
   
    dict_1 = {}

    person = {"name": "david", "address": "mumbai", "age": 30, "Salary": 40000.45}
    person['name'] = 'dezel'
    person['country'] = 'India'
    print(f"person = {person}, type = {type(person)}")
    del person['country']
  

    print(f"list of keys: {list(person)}")    
    print(f"check name key in person: {'name' in person}")

    # merge two dictionaries using '**' and '|'
    new_person = {**person, "blood_group":"O positive"}
    print(new_person)
    
    new_person = person | {"blood_group": "AB +"}
    print(f"merged dict = {new_person}")
    
    # stores list of tuples whre each tuple represents 1st item key and 2nd item as value
    persons = [person.items()]
    print(type(persons), persons)


# function_2()