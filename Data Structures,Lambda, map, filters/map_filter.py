
def func_1():
    numbers = list(range(1, 11))
    # print(numbers)
    
    # map is iterating 'numbers' collection using for loop i.e. 
    # passing each element to function and returns result
    sqaures = list(map(lambda x: x ** 2, numbers))
    print(sqaures)
    
    evens = list(filter(lambda x: x % 2 == 0, numbers))
    print(evens)
    
    even_sqaures = list(map(lambda x: x ** 2, filter(lambda x: x % 2 == 0, numbers)))
    print(even_sqaures)
    
func_1()