# functions
"""
1. A function lets us create a set of instructions that we can run when needed.
Functions are essential in Python and in many other programming languages. 
They help us create meaningful programs, because they allow us to decompose a 
program into manageable parts, and they promote readability and code reuse

2. A docstring (short for "documentation string") in Python is a 
string literal that occurs as the first statement in a module, function, 
class, or method definition. Its purpose is to provide documentation about 
the object it belongs to.

Note: 
    If we have 10 params in function and want to pass only specific params 
    in function call first all params should have defuolt values 
    and we can pass positionalm params in any sequence
"""
# function bydefault returns None if return statement is absent

# empty function
def function_1():
    """
    this is just empty function
    this should have aleast one statement to compensate for indentation
    """
    pass



# function_1()

# print doc string
print(f" doc string for function_1: {function_1.__doc__}")


# parameterless function
def function_2():
    """
    this is test function 2
    this is not taking any parameters
    """
    print("inside function 2")


# function_2()


# parametrized function
def function_3(param):
    """
    this is paramterized function
    which is taking argument
    """
    print(f"inside function 3")
    print(f"param = {param}, type of param = {type(param)}")


# function_3(10)  # ok int
# function_3(1.54)  # ok float
# function_3("hello")  # ok str
# function_3(None)  # ok None
# function_3(True)
# function_3(1245435354.45367676765574355) # float
# 'None' keyword is used to represent null value or no value.


def function_4(param):
    print(f"inside function 4")
    print(f"{param} is passed, type = {type(param)}")


# function_4(print) # built in function is passed
# function_4(function_3(function_2()))

def function_1(name="no name", age=20):
    """
    This function has default arguments
    """
    print("Inside function 2")
    print(f"name = {name}, type = {type(name)}")
    print(f"age = {age}, type = {type(age)}")

# function_1()

#overwrite default arg with new ones
# function_1("person",30) 

# positional parameters

# function_1(name="person3")


def function_5(*args):
    """
    Arbitary Arguments: Used when no of arguments to be passed are not known

    Returns
    -------
    None.

    """
    print(f"type: {type(args)}")
    print([i for i in args])
    
# below arguments are wrapped up in the tuple
# function_5("omkar", "ellicium", 254)

def function_6(**kwargs):
    """
    Keyword Arguments: Pass arguments as key=value
    This way the order of arguments does not matter
    It stores data in dictionary format
    

    Returns
    -------
    None.

    """
    print(f"{type(kwargs)}")
    print([i for i in kwargs])
    
# Arguments are passed as dict(Key-Value pairs)
# function_6(code=123, company="Eiilicium", location="Kothrud")

# print(type(function_6))
    