"""
for and while loops are used to iterate over iterable(collections)
For Loop Execution:
    1. Initialize loop variable to 1st value of list/string/range
    2. executes body of the loop for each value in sequence
    3. loop terminates when it reaches at the end of sequence
    4. After each iteration loop variable is assigned to next value in sequence

while loop execution:
    A Python while loop only runs when the condition is met.
    1st condition specified is evaluated
    Loop body will get executed
    After each iteration, condition is checked again
    Keeps on iterating till specified condition is false
"""

def func1():
    # sample for loop
    # for value in  range(0, 5):
    #     print(value)
    
    # for loop in single line
    numbers = [12, 43, 21, 79, 5, 6]
    for num in numbers: print(num)
    
    # addition of all elements
    res = 0
    for num in numbers:
        res += num
    print(f"sum = {res}")
    
    print(f"sum = {sum(numbers)}")
    
    # else block in for..in loop gets called only when the for loop does not break
    for value in range(5):
        print(f"value = {value}")
        # break from loop when if condition is true
        if value > 3:
            break
        else:
            print("this is for's else block")
    
    # in case if you want to play around indexing
    numbers = [123, 45, 67]
    for i in range(0, len(numbers)):
        print(numbers[i])
    
        
func1()