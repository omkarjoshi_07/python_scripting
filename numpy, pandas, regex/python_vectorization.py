import numpy as np
import pandas as pd
import time

def add(a,b):
  return a + b

def func1():
    """
    Vectorization using numpy
    """
    # Create two arrays
    a = np.array([10, 25, 29, 256])
    b = np.array([5, 6, 7, 8])
    
    # Add corresponding elements of two arrays
    # broadcasting in numpy is also vectorization
    addition = a + b
    print(addition )  
    
    # find sqaure root 
    square_root = np.sqrt(a)
    print(f"Square Root: {square_root}")
    
    # find exponential of each element of the array
    exponential = np.exp(b)
    print(exponential)
    
    # addition using vectorize
    # The purpose of np.vectorize is to transform functions which are not 
    # numpy-aware (e.g. take floats as input and return floats as output)
    # into functions that can operate on (and return) numpy arrays.
    Vecfunc = np.vectorize(add,otypes=[float])
    print(Vecfunc(1,8))
    
    
# func1()


def func2():
    """
    Pandas vectorization
    """
    # Create a Pandas DataFrame
    df = pd.DataFrame({
        'A': [1, 2, 3, 4],
        'B': [5, 6, 7, 8],
        'C': [9, 10, 11, 12]
    })
    
    """
    Internally, Pandas Series are often stored as NumPy arrays, in this 
    case arrays of floats. Pandas is smart enough to pass the operation
    on to the underlying arrays. No slow Python code is involved in doing
    the arithmetic.
    """
    df['Sum'] = df['A'] + df['B']
    print(df)
    
    
    
    """
    In contrast, the non-vectorized method calls a Python function for every
    row, and that Python function does additional operations. 
    Eventually this devolves into low-level multiplication and division, 
    but there is slow and expensive Python code being called repeatedly 
    for every single row.
    """
    
    sentences = pd.Series([
    "This is a sample sentence.",
    "Pandas is great for data manipulation.",
    "Vectorization makes operations faster.",
    "I love working with Python and Pandas."
    ])

    """
    Pandas provides a .str object on Series that lets you run 
    various vectorized operations on strings.
    """
    
    word_counts = sentences.str.split().str.len()
    
    # Create a new DataFrame to display the sentences and their word counts
    sentence_word_counts = pd.DataFrame({
        'Sentence': sentences,
        'Word Count': word_counts
    })
    
    
    print(sentence_word_counts)
    

# func2()


def count_loop(X, target):
    start_time = time.time()
    count = sum(x == target for x in X["numbers"])
    end_time = time.time()
    return count, (end_time - start_time) * 1000  

def count_vectorized(X, target):
    start_time = time.time()
    count = (X["numbers"] == target).sum()
    end_time = time.time()
    return count, (end_time - start_time) * 1000 

def func3():
    
    data = {'numbers': np.random.randint(1, 10, size=1000000)}
    
    df = pd.DataFrame(data)
    
    count_loop_result, loop_time = count_loop(df, 2)
    print("Occurrences using loop:", count_loop_result)
    print("Execution time (loop): {:.3f} milliseconds".format(loop_time))
    
    # Perform counting with vectorized operation
    count_vectorized_result, vectorized_time = count_vectorized(df, 2)
    print("Occurrences using vectorized operation:", count_vectorized_result)
    print("Execution time (vectorized): {:.3f} milliseconds".format(vectorized_time))
    
    """
    In above code, count_loop() and count_vectorized(), both functions are
    counting occurrence of a particular integer in data frame column.
    by running code, it is seen that count_vectorized() function is getting
    executed faster than ciunt_loop() because of vectorization
    """

# func3()


def func4():
    data = {'Student': [f'Student_{i}' for i in range(1, 10)]}
    df = pd.DataFrame(data)
    
    print(df.head())
    

func4()

