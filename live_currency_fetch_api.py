# -*- coding: utf-8 -*-
"""
Created on Tue Dec 26 12:13:49 2023

@author: Omkar
"""

import requests
from datetime import datetime

api_key = 'abcdc'
api_url = f'https://openexchangerates.org/api/latest.json?app_id={api_key}'
response = requests.get(api_url)

if (datetime.now().weekday() < 6):
    if response.status_code // 100 == 2:
        try:
            
            # Try to parse the JSON response
            data = response.json()
            
            # Print exchange rates for USD
            exchange_rates = data['rates']
    #         print(exchange_rates)
    #         print(data)
            currencies_of_interest = ['CAD', 'INR', 'GBP', 'EUR', 'CNY']
            exchange_rates2 = {currency: data['rates'][currency] for currency in currencies_of_interest}
            # print(exchange_rates2)
            for currency, rate in exchange_rates2.items():
    #             print(currency)
                print(f'USD to {currency}: {rate}')
    #         print([[currency,rate] for c,rate in exchange_rates.items() if currency == 'CAD'])
            print("Live USD Exchange rates fetched successfully!")
        except ValueError as e:
            print(f'Error parsing JSON: {e}')
            print(f'Response Content: {response.text}')
    else:
        print(f'Error: {response.status_code} : {response.text}')
        
else:
    print(f"Oops.. Today is {datetime.now().strftime('%A')}. Try again on Monday")
    
    


