# -*- coding: utf-8 -*-


import requests
import os
from datetime import datetime
from bs4 import BeautifulSoup
from config import base_url, no_of_files_to_download
import pandas as pd

def download_files(base_url, no_of_files_to_download):
    # hit url and create response object
    response = requests.get(base_url)
    
    if response.status_code == 200:
        
        try:
            # parse html content using beautiful soup
            soup = BeautifulSoup(response.text, 'html.parser')
        
            # get first 5 text files from base_url
            text_file_links = [data['href'] for data in soup.findAll('a') if data['href'].endswith('.txt')][0:no_of_files_to_download]
            
            # Download each text file and store them in separate folder
            for i in range(len(text_file_links)):
                path = base_url + text_file_links[i]
                file_req = requests.get(path)
                # print(file_req.text)
                
                # Create directory if not exists for storing each file
                if not os.path.exists(f"file_{i+1}"):
                    os.makedirs(f"file_{i+1}")
                
                destination = f"file_{i + 1}/{text_file_links[i]}"
                file = open(destination, 'w')
                file.write(file_req.text)
                
                print(destination)
    
            
        except Exception as e:
            print(e)
            
        finally:
            file.close()
    
# download_files(base_url, no_of_files_to_download)

def set_combination(no_of_files_to_download):
    try:
        # print(os.listdir("file_1/")[0])
        for i in range(0, no_of_files_to_download):
            file_name = os.listdir(f"file_{i+1}/")[0]
            path = os.path.join(f"file_{i+1}/", file_name)
            # print(path)
            file = open(path, "r")
            
            data = file.readlines()
            
            # print([line.replace('\n', '') for line in data])
            
            # print({line:len(line.split()) for line in data})
            records_dict = {file_name:{i:len(data[i].split()) for i in range(len(data))}}
            
            print(records_dict)
        
            file.close()
            
    
    except Exception as e:
        print(e)
    
    finally:
        file.close()

# set_combination(no_of_files_to_download)

def count_words_morethan_10(no_of_files_to_download):
    try:
        # print(os.listdir("file_1/")[0])
        for i in range(0, no_of_files_to_download):
            file_name = os.listdir(f"file_{i+1}/")[0]
            path = os.path.join(f"file_{i+1}/", file_name)
            # print(path)
            file = open(path, "r")
            
            data = file.readlines()

            count_lines = 0
          
            for line in data:
                if len(line.split()) > 10:
                    count_lines += 1
            print(f"{file_name}: {count_lines}")
            
            
            file.close()
            
    
    except Exception as e:
        print(e)
    
    finally:
        file.close()

# count_words_morethan_10(no_of_files_to_download)

def find_file_details(no_of_files_to_download):
    try:
        sizes = []
        modified_dates = []
        paths = []
        files = []
        for i in range(0, no_of_files_to_download):
            file_name = os.listdir(f"file_{i+1}/")[0]
            files.append(file_name)
            print(file_name)
            file_size = os.path.getsize(os.path.join(f"file_{i+1}/", file_name))
            
            sizes.append(file_size)
            print(file_size)
            
            last_modified = datetime.fromtimestamp(os.path.getmtime(os.path.join(f"file_{i+1}/", file_name)))
            modified_dates.append(last_modified)
            print(last_modified)
            
            absolute_path = os.path.abspath(os.path.join(f"file_{i+1}/", file_name))
            paths.append(absolute_path)
            print(absolute_path)            
            
        
        data = {
                "File Name": files,
                "File Size(Bytes)": sizes,
                "Last Modified": pd.to_datetime(modified_dates),
                "Absolute Path": paths
            }
         
        
        df = pd.DataFrame(data)
        df.to_csv("./File_Details.csv", index=False)
        print(df)
            
            
    except Exception as e:
        print(e)
            
# find_file_details(no_of_files_to_download)



